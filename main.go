package main

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"git.sr.ht/~diamondburned/gocad"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/nixhub/nixhub.io/dispenser"
	"gitlab.com/nixhub/nixhub.io/templates"
	"gitlab.com/shihoya-inc/errchi"
)

func main() {
	var token string
	var err error

	// Try and load a token from file
	if file := os.Getenv("TOKEN_FILE"); file != "" {
		f, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatalln("Failed to open", file+":", err)
		}

		token = strings.TrimSpace(string(f))
	} else {
		token = os.Getenv("BOT_TOKEN")
	}

	if token == "" {
		log.Fatalln("Token must not be empty!")
	}

	var d *discordgo.Session

	start("Discord", func() {
		// nixhubd
		d, err = discordgo.New("Bot " + token)
		if err != nil {
			log.Fatalln("AAAA:", err)
		}

		d.StateEnabled = true
		d.State.TrackChannels = true
		d.State.TrackEmojis = true
		d.State.TrackMembers = true
		d.State.TrackRoles = true
		d.State.TrackVoice = false
		// dispenser keeps its own message pool
		d.State.MaxMessageCount = 0

		if err := d.Open(); err != nil {
			log.Fatalln("Failed to connect to Discord:", err)
		}
	})

	defer d.Close()

	start("Templates", templates.Initialize)

	var s *dispenser.State

	start("Dispenser", func() {
		s, err = dispenser.Initialize(d, os.Getenv("CHANNEL_ID"))
		if err != nil {
			log.Fatalln("Failed to initialize dispenser:", err)
		}
	})

	r := errchi.NewRouter()
	r.Mount(templates.MountDir("/static"))
	r.Get("/feed", s.Handler)
	r.Get("/", templates.QuickRender(nil))

	log.Println("Serving at :8080")

	if err := gocad.Serve(":8080", r); err != nil {
		log.Fatalln("Failed to start gocad:", err)
	}
}

var startedWhen = map[string]time.Time{}

func start(thing string, fn func()) {
	log.Println("Starting", thing+"...")
	t := time.Now()

	fn()

	log.Println("Started", thing+",", "took", time.Now().Sub(t))
}
